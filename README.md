# README #

* Clone repo
* Create a virtualenv (optional) 
* Install requirements.
* Get TOKEN (talk to botfather).
* Set TOKEN env var in your machine
* Set MONGODB_URI (from your local instance of mongo or from some other place like mlab)
* Run bot.py

### What is this repository for? ###

* Python Telegram Bot for automated d20 rolls calcs.

### How do I use it? ###

* Add bot to your telegram.
* Send it commands (like /roll 3d6+3) and the bot will answer you 

### Available commands ###

* /roll [roll] -> unregistered and fast roll (without need to save it)
* /register -> explicitely adds you to the db and allows you to use /save and /sroll
* /save [name] [roll] -> saves the roll for further use (if other user runs this command will not get your saved roll but hers)
* /sroll [name] -> runs your saved [roll] with the specified [name]
* /show -> shows your /srolls

### Available rolls ###

* [number_of_dices]d[type_of_dice] (+/- [modifier]) (x [critical_modifier]) ('E') 
* number_of_dices rolled (1,2,3,4, etc)
* type_of_dice d_whatever (d2, d5, d6, d10, d20, d24 etc)
* modifier extra stuff (str, feats, spells, whatever)
* critical_modifier if is a crit use it (depends of the character)
* 'E' means empowered +50% for spell metamodified that way