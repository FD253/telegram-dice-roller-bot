import re
import logging

from os import getenv
from random import randrange
from telegram.ext import Updater, CommandHandler
from schemas import User, Roll
from mongoengine import connect

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - '
                    '%(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Connect to mLab backend
connect(host=getenv('MONGODB_URI'))


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    update.message.reply_text('Mini introduction')


def help(bot, update):
    update.message.reply_text('Detailed usage info')


def escape_markdown(text):
    """Helper function to escape telegram markup symbols"""
    escape_chars = '\*_`\['
    return re.sub(r'([%s])' % escape_chars, r'\\\1', text)


def register(bot, update):
    """Register the user id. This allows to use custom commands"""
    user = update.message.from_user
    try:
        if User.objects.get(telegram_id=user.id):
            bot.send_message(text="You've already registered.\n"
                             "Check your custom commands using /show",
                             chat_id=update.message.chat.id,
                             message_id=update.message.message_id,
                             reply_to_message_id=update.message.message_id)
    except User.DoesNotExist:
        user = User(telegram_id=user.id,
                    telegram_username=user.username,
                    telegram_first_name=user.first_name)
        user.save()
        bot.send_message(text="Great! You're now registered.\n"
                         "Save your custom rolls using the /save command.",
                         chat_id=update.message.chat.id,
                         message_id=update.message.message_id,
                         reply_to_message_id=update.message.message_id)


class CustomRoll(object):
    """Empty class for monkeypatch"""
    pass


def validate_save_command(command):
    """Check if the roll to be saved is valid"""
    try:
        roll_data = re.match(r'(\d+)d(\d+)(\+|\-)?(\d*)x?(\d*)(E?)(M?)',
                             command.split(' ')[2]).groups()
        custom_roll = CustomRoll()
        custom_roll.name = command.split(' ')[1]
        custom_roll.dice = roll_data[1]
        custom_roll.times = roll_data[0]
        if roll_data[2]:
            custom_roll.modifier = roll_data[2] + roll_data[3]
        else:
            custom_roll.modifier = 0
        if roll_data[4]:
            custom_roll.critical = roll_data[4]
        else:
            custom_roll.critical = 0
        if roll_data[5]:
            custom_roll.empowered = True
        else:
            custom_roll.empowered = False
        return custom_roll
    except IndexError:
        return None


def save(bot, update):
    """This command allows a registered user to save a new custom /sroll"""
    user = update.message.from_user
    parse = validate_save_command(update.message.text)
    if parse:
        if User.objects.get(telegram_id=user.id):
            if not Roll.objects.filter(
                user=User.objects.get(telegram_id=user.id),
                name=parse.name
            ):
                roll = Roll(user=user.id,
                            name=parse.name,
                            times=parse.times,
                            dice=parse.dice)
                if parse.modifier:
                    roll.modifier = parse.modifier
                if parse.critical:
                    roll.critical = parse.critical
                if parse.empowered:
                    roll.empowered = True
                roll.save()
                bot.send_message(
                    text="Successfully saved",
                    chat_id=update.message.chat.id,
                    message_id=update.message.message_id,
                    reply_to_message_id=update.message.message_id)
            else:
                bot.send_message(
                    text="You've already created another custom roll"
                    " with the same name. Use /modify or /delete",
                    chat_id=update.message.chat.id,
                    message_id=update.message.message_id,
                    reply_to_message_id=update.message.message_id)
        else:
            bot.send_message(text="To save custom rolls you must register"
                             "first.\nYou only need to tap the /register "
                             "command.",
                             chat_id=update.message.chat.id,
                             message_id=update.message.message_id,
                             reply_to_message_id=update.message.message_id)
    else:
        bot.send_message(text="Parse error. The accepted form is "
                         "/save <name> <roll>. Example: /save rapier 1d6+2",
                         chat_id=update.message.chat.id,
                         message_id=update.message.message_id,
                         reply_to_message_id=update.message.message_id)


def sroll(bot, update):
    """Allows to use a pre saved roll"""
    try:
        user = User.objects.get(telegram_id=update.message.from_user.id)
    except User.DoesNotExist:
        bot.send_message(text="You need to register first.\n"
                         "Use the /register command.",
                         chat_id=update.message.chat.id,
                         message_id=update.message.message_id,
                         reply_to_message_id=update.message.message_id)
        return

    try:
        name = update.message.text.split(' ')[1]
    except IndexError:
        bot.send_message(text="You need to provide a custom roll name.",
                         chat_id=update.message.chat.id,
                         message_id=update.message.message_id,
                         reply_to_message_id=update.message.message_id)
        return
    try:
        roll = Roll.objects.get(user=user, name=name)
        string = str(roll.times) + 'd' + str(roll.dice)
        if roll.modifier:
            if roll.modifier >= 0:
                string += '+' + str(roll.modifier)
        if roll.critical:
            string += 'x' + str(roll.critical)
        if roll.empowered:
            string += 'E'
        vals = re.match(r'(\d+)d(\d+)(\+|\-)?(\d*)x?(\d*)(E?)(M?)',
                        string).groups()
        total = 0
        detailed_total = list()

        def calc_detailed_total():

            for i in range(int(vals[0])):
                detailed_total.append(randrange(1, int(vals[1])+1))

        if vals[4]:
            for i in range(int(vals[4])):
                calc_detailed_total()
                if vals[2]:
                    if vals[2] == '+':
                        total += int(vals[3])
                    else:
                        total -= int(vals[3])
        else:
            calc_detailed_total()
            if vals[2]:
                if vals[2] == '+':
                    total += int(vals[3])
                else:
                    total -= int(vals[3])

        total += sum(detailed_total)
        if vals[5]:
            total *= 1.5

        text = "Total: " + str(int(total)) + "\n" + \
               "Rolls: " + str(detailed_total) + '\n' + \
               "Rolls sum: " + str(sum(detailed_total)) + '\n'

        if vals[5]:
            i, d = divmod(total/3, 1)
            text += "Empowered: " + str(int(i)) + '\n'

        text += "Custom roll: " + string
        bot.send_message(text=text,
                         chat_id=update.message.chat.id,
                         message_id=update.message.message_id,
                         reply_to_message_id=update.message.message_id)

    except Roll.DoesNotExist:
        bot.send_message(text="I don't know what do you mean",
                         chat_id=update.message.chat.id,
                         message_id=update.message.message_id,
                         reply_to_message_id=update.message.message_id)


def roll(bot, update):
    """Allows to roll any dice.
    Form /roll <times>d<dice>[+-]<modifier>[x]<critical>[E].
    Examples: /roll 1d6+4
    /roll 2d8-1
    /roll 20d6E
    /roll 2d4+32x4
    """

    try:
        vals = re.match(r'(\d+)d(\d+)(\+|\-)?(\d*)x?(\d*)(E?)(M?)',
                        update.message.text.split(' ')[1]).groups()

        total = 0
        detailed_total = list()

        def calc_detailed_total():

            for i in range(int(vals[0])):
                detailed_total.append(randrange(1, int(vals[1])+1))

        if vals[4]:
            for i in range(int(vals[4])):
                calc_detailed_total()
                if vals[2]:
                    if vals[2] == '+':
                        total += int(vals[3])
                    else:
                        total -= int(vals[3])
        else:
            calc_detailed_total()
            if vals[2]:
                if vals[2] == '+':
                    total += int(vals[3])
                else:
                    total -= int(vals[3])

        total += sum(detailed_total)
        if vals[5]:
            total *= 1.5

        text = "Total: " + str(int(total)) + "\n" + \
               "Rolls: " + str(detailed_total) + '\n' + \
               "Rolls sum: " + str(sum(detailed_total)) + '\n'

        if vals[5]:
            i, d = divmod(total/3, 1)
            text += "Empowered: " + str(int(i))
        bot.send_message(text=text,
                         chat_id=update.message.chat.id,
                         message_id=update.message.message_id,
                         reply_to_message_id=update.message.message_id)

    except IndexError:
        bot.send_message(text='What? 1d6? 3d4? 5d8+4? 2d4+32x4?',
                         chat_id=update.message.chat.id,
                         message_id=update.message.message_id,
                         reply_to_message_id=update.message.message_id)


def show(bot, update):
    """Shows all the saved commands of the user who asks"""
    try:
        user = User.objects.get(telegram_id=update.message.from_user.id)
    except User.DoesNotExist:
        bot.send_message(text="You need to register first.\n"
                         "Use the /register command.",
                         chat_id=update.message.chat.id,
                         message_id=update.message.message_id,
                         reply_to_message_id=update.message.message_id)
        return
    saved_rolls = Roll.objects.filter(user=user)
    if saved_rolls:
        reply_text = "Your custom saved roll(s):\n"
        for i in saved_rolls:
            reply_text += '    '
            reply_text += i.name + ': ' + str(i.times) + 'd' + str(i.dice)
            if i.modifier:
                if i.modifier > 0:
                    reply_text += '+' + str(i.modifier)
                else:
                    reply_text += str(i.modifier)
            if i.critical:
                reply_text += 'x' + str(i.critical)
            if i.empowered:
                reply_text += 'E'
            reply_text += '\n'

        bot.send_message(text=reply_text,
                         chat_id=update.message.chat.id,
                         message_id=update.message.message_id,
                         reply_to_message_id=update.message.message_id)

    else:
        bot.send_message(text="You don't have any saved rolls. \n"
                         "Add new ones using /save <name> <roll>",
                         chat_id=update.message.chat.id,
                         message_id=update.message.message_id,
                         reply_to_message_id=update.message.message_id)


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))


def main():
    # Create the Updater and pass it your bot's token.
    updater = Updater(getenv('TOKEN'))

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(CommandHandler("roll", roll))
    dp.add_handler(CommandHandler("register", register))
    dp.add_handler(CommandHandler("save", save))
    dp.add_handler(CommandHandler("sroll", sroll))
    dp.add_handler(CommandHandler("show", show))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Block until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
