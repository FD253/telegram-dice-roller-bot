from os import getenv
from mongoengine import *

MONGODB_URI = getenv('MONGODB_URI')

connect(host=MONGODB_URI)


class User(Document):
    telegram_id = IntField(required=True, primary_key=True)
    telegram_username = StringField(unique=True)
    telegram_first_name = StringField()


class Roll(Document):
    user = ReferenceField(User, required=True)
    name = StringField(required=True, unique_with='user')
    dice = IntField(required=True)
    times = IntField(required=True)
    modifier = IntField(default=0)
    critical = IntField(default=0)
    empowered = BooleanField()
